
import React, {Fragment} from 'react';
import './App.css';
import Header from './compomnents/Header';
import Main from './compomnents/Main';
import Footer from './compomnents/Footer';

function App() {
  const title = 'Photo'
  return (
    <React.Fragment>
      <Header title={title}/>
      <Main />
      <Footer title={title} />
    </React.Fragment>
  );
}

export default App;
