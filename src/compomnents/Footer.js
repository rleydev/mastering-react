import React from "react";
import './Footer.css';
import PropTypes from 'prop-types'; 

const Footer = ({ title }) => {
    return (
      <div className="footer">
      {title ? <h1>{title}</h1> : <h1>Title</h1>}
      </div>
    );
};

Footer.propTypes = {
  title: PropTypes.string
};

export default Footer
