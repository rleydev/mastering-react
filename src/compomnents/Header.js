import React from "react";
import './Header.css';
import PropTypes from 'prop-types'; 

const Header = ({ title }) => {
    return (
      <div className="header">
        {title ? <h1>{title}</h1> : <h1>Title</h1> }
      </div>
    );
};

Header.propTypes = {
    title: PropTypes.string
};

export default Header