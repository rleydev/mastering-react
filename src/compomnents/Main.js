import React, { useEffect } from "react";
import './Main.css';
import Photos from "./Photos";


const Main = () => {
    return (
        <div className="main">
            <Photos/>
        </div>
    )
}

export default Main;