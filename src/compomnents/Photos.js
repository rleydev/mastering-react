import './Photos.css';
import React, {useEffect} from 'react';
import data from '../data.json';

const Photos = () => {

    const photosData = data

    useEffect(() => {
        console.log(photosData)
    }, [])

    let displayedPhotos = photosData.slice(0, 10);
  
    return (
      <div className="photos">
        {displayedPhotos.map(photo => (
          <div key={photo.id} className="photo">
            <h2>{photo.title}</h2>
            <img src={photo.url} alt={photo.title} />
          </div>
        ))}
      </div>
    );
  };

  export default Photos;